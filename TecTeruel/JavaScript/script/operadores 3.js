//trabalhando com estrutura de decisao
/*
de acordo com o resultado de uma operação lógica 
um bloco de programação poderá ou não ser executado na 
estrutura if o primeiro bloco sempre pra o resultado verdadeiro(true)
*/
var n1=10, n2=5;
//qual o maior?
    if(n1>n2){//bloco para verdadeiro
	   console.log(n1+" é o maior");
     }
       else{//bloco para falso
       console.log(n2+"é o maior");
   	  } 
//comparação de strings
	  var nome1="dorgival",nome2="dorgival";
	      if(nome1==nome2){
		    console.log("nomes iguais");
			}
			else{
			console.log("nomes diferentes");
			}
/*if encadeado
//apos uma saida qualquer,outra operação lógica
podera ser feito implicando em novas programações*/
var n3=-5,n4=-5;
    if(n3>n4){ 			
	     console.log(n3+"é maior que"+n4);
		 }
		 else if(n4>n3){
		      console.log(n4+"é maior que"+n3);
			  }
			   else{
			       console.log("valores iguais");
}
 //proximo nivel:operador ternario
//forma simplificada de usar o if
//(operação logica)?saida V: saida f;
//aplicando ao exemplo anterior
     console.log(n1>n2?n1+"maior":n2+" é maior");
//estrutura de seleção-switch
//de acordo com o valor escolhido uma programação será executada
var cliente="preferencial";
    switch(cliente){
        case"preferencial":
		   console.log("não precisa de fila");
        break;		
		case"teenage":
		   console.log("fica na fila");
        break;		
		case"young":
		   console.log("fila com musica");
        break;		
		case"old man":
		   console.log("espera sentado");
        break;		
		default:
		    console.log("cliente nao identificado");
			}
//estruturas de repetição
//			