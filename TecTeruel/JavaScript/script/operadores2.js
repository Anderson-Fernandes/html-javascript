//trabalhando com operadores se atribuição
var n1=2,n2=4,n3=7,n4=-3,teste="escola";
n1++;//equivale a n1=n1+1;
n1++;
n1++;
n1++;
console.log(n1);
n4--;//equivalente a n4=n4-1;
n4--;
console.log(n1*n4);
n2+=n1;//n2=n2+n1;
n1+=13;//n1=n1+13;
console.log(n2);//10
console.log(n1);//19
n3-=20;//n3=n3-20;
console.log(n3);
n3*=3;//-13
console.log(n3);//-39
n4/=2;//n4=n4/2
console.log(n4);//2.5
//operador Módulo mod%
console.log(n3%10);//?
//operador Relacional
console.log(n1+" _ "+n4);
console.log("n1==n4?"+(n1==n4));
console.log("n1!=n4?"+( n1!=n4));
console.log("n1>n4? "+(n1>n4));
console.log("n1<n4? "+(n1<n4));
//operadores logicos
//E &&
console.log(n1+","+n2+","+n3+","+n4);
console.log(n1>n3&&n1<n4);//19>39&&19<-2.5
console.log(n2!=n1&&n4>(n2+n3));//10!=19&&-2.5>-29
//ou ||
console.log(n1>n3||n1<n4);
console.log(n2!=n1||n4>(n2+n3));

