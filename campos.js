	function enviardados(){ 
		if(document.dados.txt_nome.value == "" || document.dados.txt_nome.value.length < 8) { 
			alert( "Preencha campo NOME corretamente!" ); 
			document.dados.txt_nome.focus(); 
			return false; //não envia os dados
		} 
		if( document.dados.txt_email.value=="" || document.dados.txt_email.value.indexOf('@')==-1 || document.dados.txt_email.value.indexOf('.')==-1 ) { 
			alert( "Preencha campo E-MAIL corretamente!" ); 
			//posiciona o cursor na caixa de email
			document.dados.txt_email.focus(); 
			return false; //não envia os dados
		} 
		if (document.dados.txt_mensagem.value=="") { 
			alert( "Preencha o campo MENSAGEM!" ); 
			document.dados.txt_mensagem.focus(); 
			return false; 
		} 
		if (document.dados.txt_mensagem.value.length < 50 ) { 
			alert( "É necessario preencher o campo MENSAGEM com mais de 50 caracteres!" ); 
			document.dados.txt_mensagem.focus(); 
			return false; 		
		}
		
		//cpf
		if (document.dados.txt_cpf.value.length < 11 ) { 
			alert( "Preencha campo CPF corretamente!" ); 
			document.dados.txt_cpf.focus(); 
			return false; 
		}
		
		return true; 
	} 
	