var n1, n2,n3;
document.writeln ("Laço de Repetição - FOR");
n1 = parseInt(prompt("Digite um valor numérico inicial"));
n2 = parseInt(prompt("Digite um valor numérico Final"));

/*A estrutura de repetição "FOR" possui um valor de inicio
e um valor de fim que controlam as repetições */

	for(var i = n1; i<n2; i++) //for (var i=n1; i<n2; i+=3) **PULAR EM 3 EM 3**
	{
		document.writeln(i);
	}
	
/*A estrutura de repetição "WHILE" testa uma condição	
Antes de iniciar o processo de repetição
Enquanto essa condição for verdadeira ocorrerá
A Repetição */

document.writeln("<p>Laço de Repetição - WHILE</p>");
	var cont=n1;
	while(cont<n2)
	{
		document.writeln(cont);
		cont++;
	}
	
/* "DO WHILE" */

	document.writeln("<p>Laço de Repetição - DO WHILE</p>");
	cont = n1;
	do
	{
		document.writeln(cont);
		cont++;
	}
	while (cont<n2);